# MOVIE API REST
El proyecto actual es una API REST que permite gestionar y consultar películas y sus géneros.

Para gestionar las dependencias se ha implementado Maven; los tests unitarios han sido desarrollados con jUnit y se encuentran en la carpeta **test**.

## Contenido
Este proyeto tiene dos ramas: develop y main, **develop** contiene el código en desarrollo y **main** contiene una versión estable del mismo.

## Instalación
Para ejecutar este proyecto se debe indicar como Main Class la clase: **com.api.ApiMovieApplication**

## Llamadas HTTP

Al ejecutar la aplicación se desplegará automáticamente una base de datos H2 con registros insertados que se encuentran en la carpeta **resources** en el archivo **data-h2.sql**.

Una vez esté el proyecto en ejecución se pueden realizar peticiones.

Para ver las peticiones al detalle visita 
 [aquí](https://gitlab.com/anthonny17junio/movie-api/-/wikis/End-points).

## Postman
En la carpeta **resources** se ha añadido el archivo **postman.json** el cual es una colección Postman con todos los endpoints disponibles.

Para poder realizar las llamadas se deben realizar los siguientes pasos: 
1. Lanzar la petición a **Token Admin** o **Token User**, dependiendo que llamada se realice se obtendrá un token de administrador (puede realizar todas las peticiones) o un token de usuario (peticiones limitadas).
2. Al lanzar dicha petición, se recibirá un JSON con tres campos ("issuedAt","expiration" y "token")
3. Copiar el valor completo del campo **token** (empezará por "Bearer")
4. Crear una variable global llamada **token** y en el valor de **CURRENT VALUE** pegar el token generado
5. Listo! Ya puedes lanzar cualquier petición.