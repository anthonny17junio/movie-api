package com.api.controller;

import com.api.configuration.exception.UnprocessableEntityException;
import com.api.dto.GenreDto;
import com.api.dto.InsertGenreDto;
import com.api.model.Genre;
import com.api.service.GenreService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.api.configuration.Constants.GENRE;
import static com.api.configuration.Constants.URL_GENRE;

/**
 * Controlador de Géneros
 */
@Getter
@Setter
@Api(tags = GENRE)
@RestController
public class GenreController {

    private static final Logger LOGGER = LogManager.getLogger(GenreController.class);

    @Autowired
    private GenreService genreService;

    /**
     * Obtiene todos los géneros disponibles
     *
     * @return Respuesta HTTP OK con todos los géneros
     */
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    @GetMapping(value = URL_GENRE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get all genres", response = List.class)
    public ResponseEntity<List<GenreDto>> getAll() {
        LOGGER.info("GET: " + URL_GENRE);
        return genreService.getAll();
    }

    /**
     * Obtiene un género dado un id
     *
     * @return Respuesta HTTP OK con el género o respuesta HTTP NOT_FOUND si no existe un género con dicho id
     */
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    @GetMapping(value = URL_GENRE + "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get one genre by id", response = Genre.class)
    public ResponseEntity<GenreDto> getById(
            @PathVariable long id
    ) {
        LOGGER.info("GET: " + URL_GENRE + "/" + id);
        return genreService.getById(id);
    }

    /**
     * Inserta un nuevo género
     *
     * @param genreDto Género a insertar
     * @return Respuesta HTTP OK si se inserta correctamente
     * @throws UnprocessableEntityException
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = URL_GENRE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> createGenre(@Valid @RequestBody InsertGenreDto genreDto)
            throws UnprocessableEntityException {
        LOGGER.info("POST: " + URL_GENRE);
        return genreService.createGenre(genreDto);
    }

    /**
     * Elimina un género dado un id
     *
     * @return Respuesta HTTP OK si se elimina o NOT_FOUND si no existe un género con dicho id
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = URL_GENRE + "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Delete one genre by id", response = Genre.class)
    public ResponseEntity<Void> deleteById(
            @PathVariable long id
    ) {
        LOGGER.info("DELETE: " + URL_GENRE + "/" + id);
        return genreService.deleteById(id);
    }
}
