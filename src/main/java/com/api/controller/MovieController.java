package com.api.controller;

import com.api.configuration.exception.GenreNotFoundException;
import com.api.configuration.exception.NoValidDirectionException;
import com.api.configuration.exception.NoValidSortMovieFieldException;
import com.api.configuration.exception.UnprocessableEntityException;
import com.api.dto.InsertMovieDto;
import com.api.dto.MovieDto;
import com.api.model.Movie;
import com.api.service.MovieService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;

import java.util.List;
import java.util.Optional;

import static com.api.configuration.Constants.*;

/**
 * Controlador de películas
 */
@Getter
@Setter
@Validated
@Api(tags = MOVIE)
@RestController
public class MovieController {

    private static final Logger LOGGER = LogManager.getLogger(MovieController.class);

    @Autowired
    private MovieService movieService;

    /**
     * Obtiene un listado de películas de modo paginado
     *
     * @param sortList         Lista de campos y tipo (asc,desc) de orden a aplicar en la paginación.
     *                         Sigue el siguiente formato: ["campo1,tipo1","campo2,tipo2"]
     * @param genresFilterList Lista de id de géneros a filtrar
     * @param page             Número de página (0 por defecto)
     * @param size             Número de elementos por página (10 por defecto, 100 máximo)
     * @return Respuesta HTTP con Lista de películas
     * @throws NoValidDirectionException      Si el tipo de orden (asc,desc) no es correcto
     * @throws NoValidSortMovieFieldException Si el campo al que aplicar el orden no existe
     */
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    @GetMapping(value = URL_MOVIE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get list of movies", response = List.class)
    public ResponseEntity<List<MovieDto>> getMovies(
            @RequestParam(defaultValue = "id,desc", name = "sort") String[] sortList,
            @RequestParam(name = "genre") Optional<Long[]> genresFilterList,
            @RequestParam(required = false, defaultValue = DEFAULT_PAGE) int page,
            @RequestParam(required = false, defaultValue = DEFAULT_PAGE_SIZE) @Max(MAX_PAGE_SIZE) int size
    ) throws NoValidDirectionException, NoValidSortMovieFieldException {
        LOGGER.info("GET: " + URL_GENRE);
        return movieService.getMovies(sortList, genresFilterList, page, size);
    }


    /**
     * Obtiene una película dado un id
     *
     * @return Respuesta HTTP OK con la película o respuesta HTTP NOT_FOUND si no existe una película con dicho id
     */
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    @GetMapping(value = URL_MOVIE + "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get one movie by id", response = Movie.class)
    public ResponseEntity<MovieDto> getById(
            @PathVariable long id
    ) {
        LOGGER.info("GET: " + URL_MOVIE + "/" + id);
        return movieService.getById(id);
    }

    /**
     * Inserta un nuevo película
     *
     * @param movieDto Película a insertar
     * @return Respuesta HTTP OK si se inserta correctamente
     * @throws UnprocessableEntityException Si existe algún error al procesar la entidad
     * @throws GenreNotFoundException       Si el género indicado no existe
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = URL_MOVIE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> createMovie(@Valid @RequestBody InsertMovieDto movieDto)
            throws UnprocessableEntityException, GenreNotFoundException {
        LOGGER.info("POST: " + URL_MOVIE);
        return movieService.createMovie(movieDto);
    }

    /**
     * Elimina un película dado un id
     *
     * @return Respuesta HTTP OK si se elimina o NOT_FOUND si no existe un película con dicho id
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = URL_MOVIE + "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Delete one movie by id", response = Movie.class)
    public ResponseEntity<Void> deleteById(
            @PathVariable long id
    ) {
        LOGGER.info("DELETE: " + URL_MOVIE + "/" + id);
        return movieService.deleteById(id);
    }
}
