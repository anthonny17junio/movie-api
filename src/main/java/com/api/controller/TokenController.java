package com.api.controller;

import com.api.model.Token;
import com.api.model.User;
import com.api.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;

import static com.api.configuration.Constants.*;

/**
 * Controlador de Tokens
 */
@Getter
@Setter
@Api(tags = TOKEN)
@RestController
public class TokenController {

    private static final Logger LOGGER = LogManager.getLogger(TokenController.class);

    @Autowired
    private UserService userService;

    /**
     * Obtiene un token dado un usuario
     *
     * @param userName Nombre de usuario
     * @return Respuesta con el token generado
     */
    @GetMapping(value = URL_TOKEN, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get authorization token", response = Token.class)
    public ResponseEntity<Token> getToken(@NotBlank @RequestHeader("user-name") String userName) {

        LOGGER.info(URL_TOKEN + "/?cn=" + userName);
        User user = userService.loadUserByUsername(userName);

        Token token = userService.getJWTToken(user);
        if (token != null) {
            LOGGER.info("Token generated");
            return ResponseEntity.status(HttpStatus.OK).body(token);
        }
        throw new IllegalArgumentException();
    }

}
