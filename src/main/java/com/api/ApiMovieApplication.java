package com.api;

import com.api.properties.SecurityProperties;
import com.api.properties.KafkaProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import static com.api.configuration.Constants.PACKAGE_MODEL;

@SpringBootApplication
@EntityScan({PACKAGE_MODEL})
@EnableConfigurationProperties({SecurityProperties.class, KafkaProperties.class})
public class ApiMovieApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiMovieApplication.class, args);
    }

}
