package com.api.model;

import com.api.configuration.Constants;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@ApiModel(value = Constants.ERROR_ELEMENT)
public class ErrorElement {

    private Integer position;
    private String movieId;
    private List<String> errors;
}
