package com.api.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Error {

    AUTHENTICATE("Authenticate"),
    USER_NOT_FOUND("User not found or valid"),
    ENTITY_ID_NOT_FOUND("Entity ID not found"),
    MISSING_REQUIRED_PROPERTY("Missing required property"),
    ENTITY_NOT_PROCESSED("The entity received cannot be processed"),
    INCORRECT_TOKEN("Incorrect token received"),
    EXPIRED_TOKEN("Expired token received"),
    ERROR_TOKEN("There is an error with the token provided. Please request a new one"),
    MISSING_TOKEN("No token received"),
    BODY_ERROR("Required request body is missing"),
    EVENT_EXPIRED("Event is marked as expired by timestamp"),
    MISSING_REQUEST_HEADER("Missing request header"),
    ACCESS_DENIED_ROLE("Permission denied. Role assigned to user missing"),
    UNIQUE_KEY_VIOLATION("Key violated. You can not delete a genre if one ore more movies are referenced to it."),
    MULTIPLE_ERRORS("There is an error in one or more elements of the list"),
    GENRE_NOT_FOUND("Genre not found"),
    MAX_PAGE_SIZE_VIOLATION("Max page size violation"),
    NO_VALID_DIRECTION("Direction value is not valid"),
    NO_VALID_MOVIE_FIELD("Sort movie field is not valid"),
    INTERNAL_ERROR("Internal error");

    private String message;

}
