package com.api.model;

import com.api.configuration.Constants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
@ApiModel(value = Constants.TOKEN)
@Builder
public class Token {

    @ApiModelProperty(example = "2021-04-19T11:23:37.473Z")
    private ZonedDateTime issuedAt;

    @ApiModelProperty(example = "2021-04-19T12:23:37.473Z")
    private ZonedDateTime expiration;

    @ApiModelProperty(example = "Bearer eyJhbGciOiJIUzUxMiJ9.e...")
    private String token;

}
