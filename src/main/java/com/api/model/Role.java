package com.api.model;

import com.api.configuration.Constants;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = Constants.TABLE_ROLES)
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

}
