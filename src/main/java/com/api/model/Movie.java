package com.api.model;

import com.api.configuration.Constants;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.*;

/**
 * Entidad de una película
 */
@Data
@Entity
@Table(name = Constants.TABLE_MOVIES)
@ApiModel(value = Constants.MOVIE)
public class Movie {

    /**
     * Id autogenerado de la película
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Título de la película
     */
    @Column
    private String title;

    /**
     * Rating de la película
     */
    @Column
    private Float rating;

    /**
     * Año de estreno
     */
    @Column
    private Integer year;

    /**
     * Un mismo género puede pertenecer a varias películas
     */
    @ManyToOne
    @JoinColumn(name = "genre_id", nullable = false)
    private Genre genre;


}
