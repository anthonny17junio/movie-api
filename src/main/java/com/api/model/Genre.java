package com.api.model;


import com.api.configuration.Constants;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = Constants.TABLE_GENRES)
@ApiModel(value = Constants.GENRE)
public class Genre {
    /**
     * Id autogenerado del género
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Nombre del género
     */
    @Column
    private String name;
}
