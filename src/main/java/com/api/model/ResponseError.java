package com.api.model;

import com.api.configuration.Constants;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.List;

@Getter
@Setter
@ApiModel(value = Constants.RESPONSE_ERROR)
public class ResponseError {

    private Integer status;
    private Integer code;
    private String message;
    private List<ErrorElement> errorElements;

    public ResponseError(HttpStatus status, Error error) {
        this.status = status.value();
        this.code = error.ordinal();
        this.message = error.getMessage();
    }

    public ResponseError(HttpStatus status, Error error, String message) {
        this.status = status.value();
        this.code = error.ordinal();
        this.message = message;
    }

    public ResponseError(HttpStatus status, Error error, List<ErrorElement> errorElements) {
        this.status = status.value();
        this.code = error.ordinal();
        this.message = error.getMessage();
        this.errorElements = errorElements;
    }
}
