package com.api.dto;

import com.api.configuration.Constants;
import io.swagger.annotations.ApiModel;
import lombok.*;

/**
 * DTO de consulta de un género
 */
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = Constants.GENRE)
public class GenreDto {

    /**
     * Id del género
     */
    private Long id;

    /**
     * Nombre del género
     */
    private String name;
}
