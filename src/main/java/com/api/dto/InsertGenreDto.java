package com.api.dto;

import com.api.configuration.Constants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotNull;

/**
 * DTO de inserción de un género
 */
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = Constants.GENRE)
public class InsertGenreDto {

    @ApiModelProperty(required = true)
    @NotNull
    private String name;
}
