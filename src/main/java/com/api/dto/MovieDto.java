package com.api.dto;

import com.api.configuration.Constants;
import io.swagger.annotations.ApiModel;
import lombok.*;

/**
 * DTO de consulta de una película
 */
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = Constants.MOVIE)
public class MovieDto {

    /**
     * Id de la película
     */
    private Long id;

    /**
     * Título de la película
     */
    private String title;

    /**
     * Rating de la película
     */
    private Float rating;

    /**
     * Año de la primera película del mundo
     */
    private Integer year;

    /**
     * Id del género al que pertenece la película
     */
    private Long genreId;
}
