package com.api.dto;

import com.api.configuration.Constants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * DTO de inserción de una película
 */
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = Constants.MOVIE)
public class InsertMovieDto {

    /**
     * Título de la película
     */
    @ApiModelProperty(required = true)
    @NotNull
    private String title;

    /**
     * Rating de la película, máximo 5 estrellas
     */
    @Min(0)
    @Max(5)
    private Float rating;

    /**
     * Año de la primera película del mundo
     */
    @Min(1895)
    @Max(3000)
    private Integer year;

    /**
     * Id del género al que pertenece la película
     */
    @ApiModelProperty(required = true)
    @NotNull
    private Long genreId;
}
