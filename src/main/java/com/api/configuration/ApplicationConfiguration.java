package com.api.configuration;

import com.api.dto.GenreDto;
import com.api.dto.InsertGenreDto;
import com.api.model.Genre;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableCaching
public class ApplicationConfiguration {


    @Bean
    public ModelMapper modelMapperModelToDto() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.addMappings(new PropertyMap<Genre, GenreDto>() {
            @Override
            protected void configure() {

            }
        });
        return modelMapper;
    }

    @Bean
    public ModelMapper modelMapperDtoToModel() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.addMappings(new PropertyMap<InsertGenreDto, Genre>() {
            @Override
            protected void configure() {

            }
        });
        return modelMapper;
    }
}
