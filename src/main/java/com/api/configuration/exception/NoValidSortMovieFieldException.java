package com.api.configuration.exception;

import org.springframework.stereotype.Component;

@Component
public class NoValidSortMovieFieldException extends Exception {
}
