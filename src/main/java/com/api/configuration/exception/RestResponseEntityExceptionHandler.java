package com.api.configuration.exception;

import com.api.model.Error;
import com.api.model.ResponseError;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import org.hibernate.PropertyValueException;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @Autowired
    LogPrinter logPrinter;

    @ExceptionHandler(value = {IllegalArgumentException.class})
    protected ResponseEntity<ResponseError> illegalArgumentExceptionHandler(IllegalArgumentException e,
                                                                            WebRequest request) {
        if (e != null && e.getMessage() != null && e.getMessage().contains("JWT")) {
            logPrinter.printLogException(request, HttpStatus.BAD_REQUEST, Error.MISSING_TOKEN);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new ResponseError(HttpStatus.BAD_REQUEST, Error.MISSING_TOKEN));
        }
        if (e != null && e.getMessage() != null && e.getMessage().contains("requestHeader.cn")) {
            logPrinter.printLogException(request, HttpStatus.BAD_REQUEST, Error.MISSING_REQUEST_HEADER);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new ResponseError(HttpStatus.BAD_REQUEST, Error.MISSING_REQUEST_HEADER));
        }
        logPrinter.printLogException(request, HttpStatus.INTERNAL_SERVER_ERROR, Error.INTERNAL_ERROR);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new ResponseError(HttpStatus.INTERNAL_SERVER_ERROR, Error.INTERNAL_ERROR));
    }

    @ExceptionHandler(NullPointerException.class)
    protected ResponseEntity<ResponseError> nullPointerExceptionHandler(NullPointerException e, WebRequest request) {
        logPrinter.printLogException(request, HttpStatus.INTERNAL_SERVER_ERROR, Error.INTERNAL_ERROR);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new ResponseError(HttpStatus.INTERNAL_SERVER_ERROR, Error.INTERNAL_ERROR));
    }

    @ExceptionHandler(value = {DataIntegrityViolationException.class})
    protected ResponseEntity<ResponseError> dataIntegrityViolationExceptionHandler(WebRequest request, Exception e) {
        logPrinter.printLogException(request, HttpStatus.BAD_REQUEST, Error.UNIQUE_KEY_VIOLATION);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ResponseError(HttpStatus.BAD_REQUEST, Error.UNIQUE_KEY_VIOLATION));
    }

    @ExceptionHandler(value = {org.hibernate.exception.ConstraintViolationException.class})
    protected ResponseEntity<ResponseError> constraintViolationExceptionHandler(ConstraintViolationException e,
                                                                                WebRequest request) {
        logPrinter.printLogException(request, HttpStatus.BAD_REQUEST, Error.ENTITY_ID_NOT_FOUND);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ResponseError(HttpStatus.BAD_REQUEST,
                        Error.ENTITY_ID_NOT_FOUND));
    }

    @ExceptionHandler(value = {javax.validation.ConstraintViolationException.class})
    protected ResponseEntity<ResponseError> constraintValidationViolationExceptionHandler(
            javax.validation.ConstraintViolationException e, WebRequest request) {
        logPrinter.printLogException(request, HttpStatus.BAD_REQUEST, Error.MAX_PAGE_SIZE_VIOLATION);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ResponseError(HttpStatus.BAD_REQUEST,
                        Error.MAX_PAGE_SIZE_VIOLATION));
    }

    @ExceptionHandler(value = {PropertyValueException.class})
    protected ResponseEntity<ResponseError> propertyValueExceptionHandler(WebRequest request) {
        logPrinter.printLogException(request, HttpStatus.BAD_REQUEST, Error.MISSING_REQUIRED_PROPERTY);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ResponseError(HttpStatus.BAD_REQUEST, Error.MISSING_REQUIRED_PROPERTY));
    }

    @ExceptionHandler(value = {UsernameNotFoundException.class})
    protected ResponseEntity<ResponseError> usernameNotFoundExceptionHandler(WebRequest request) {
        logPrinter.printLogException(request, HttpStatus.UNAUTHORIZED, Error.USER_NOT_FOUND);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                .body(new ResponseError(HttpStatus.UNAUTHORIZED, Error.USER_NOT_FOUND));
    }

    @ExceptionHandler(value = {SignatureException.class})
    protected ResponseEntity<ResponseError> signatureExceptionHandler(WebRequest request) {
        logPrinter.printLogException(request, HttpStatus.BAD_REQUEST, Error.INCORRECT_TOKEN);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ResponseError(HttpStatus.BAD_REQUEST, Error.INCORRECT_TOKEN));
    }

    @ExceptionHandler(value = {ExpiredJwtException.class})
    protected ResponseEntity<ResponseError> expiredJwtExceptionHandler(WebRequest request) {
        logPrinter.printLogException(request, HttpStatus.BAD_REQUEST, Error.EXPIRED_TOKEN);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ResponseError(HttpStatus.BAD_REQUEST, Error.EXPIRED_TOKEN));
    }

    @ExceptionHandler(value = {UnsupportedJwtException.class, MalformedJwtException.class})
    protected ResponseEntity<ResponseError> jwtExceptionHandler(WebRequest request) {
        logPrinter.printLogException(request, HttpStatus.BAD_REQUEST, Error.ERROR_TOKEN);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ResponseError(HttpStatus.BAD_REQUEST, Error.ERROR_TOKEN));
    }

    @ExceptionHandler(value = {UnprocessableEntityException.class})
    protected ResponseEntity<ResponseError> unprocessableEntityExceptionHandler(WebRequest request) {
        logPrinter.printLogException(request, HttpStatus.BAD_REQUEST, Error.ENTITY_NOT_PROCESSED);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ResponseError(HttpStatus.BAD_REQUEST, Error.ENTITY_NOT_PROCESSED));
    }

    @ExceptionHandler(value = {EventExpiredException.class})
    protected ResponseEntity<ResponseError> eventExpiredExceptionHandler(WebRequest request) {
        logPrinter.printLogException(request, HttpStatus.BAD_REQUEST, Error.EVENT_EXPIRED);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ResponseError(HttpStatus.BAD_REQUEST, Error.EVENT_EXPIRED));
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException e,
                                                                  HttpHeaders headers, HttpStatus status,
                                                                  WebRequest request) {
        logPrinter.printLogException(request, HttpStatus.BAD_REQUEST, Error.ENTITY_NOT_PROCESSED);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ResponseError(HttpStatus.BAD_REQUEST, Error.ENTITY_NOT_PROCESSED));
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException e,
                                                                  HttpHeaders headers, HttpStatus status,
                                                                  WebRequest request) {
        List<String> responseErrors = new ArrayList<>();
        e.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            responseErrors.add(fieldName.concat(": ").concat(errorMessage));
        });

        logPrinter.printLogException(request, HttpStatus.BAD_REQUEST, Error.MISSING_REQUIRED_PROPERTY);
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(new ResponseError(HttpStatus.BAD_REQUEST, Error.MISSING_REQUIRED_PROPERTY, responseErrors.toString()));
    }

    @ExceptionHandler(value = {MissingRequestHeaderException.class})
    protected ResponseEntity<ResponseError> missingRequestHeaderExceptionHandler(MissingRequestHeaderException e,
                                                                                 WebRequest request) {
        logPrinter.printLogException(request, HttpStatus.BAD_REQUEST, Error.MISSING_REQUEST_HEADER);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ResponseError(HttpStatus.BAD_REQUEST, Error.MISSING_REQUEST_HEADER, e.getMessage()));
    }

    @ExceptionHandler(value = {GenreNotFoundException.class})
    protected ResponseEntity<ResponseError> missingRequestHeaderExceptionHandler(GenreNotFoundException e,
                                                                                 WebRequest request) {
        logPrinter.printLogException(request, HttpStatus.BAD_REQUEST, Error.GENRE_NOT_FOUND);
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new ResponseError(HttpStatus.BAD_REQUEST, Error.GENRE_NOT_FOUND, Error.GENRE_NOT_FOUND.getMessage()));
    }

    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException e,
                                                                     HttpHeaders headers, HttpStatus status,
                                                                     WebRequest request) {
        logPrinter.printLogException(request, HttpStatus.BAD_REQUEST, Error.MISSING_REQUEST_HEADER);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ResponseError(HttpStatus.BAD_REQUEST, Error.MISSING_REQUEST_HEADER, e.getMessage()));
    }

    @ExceptionHandler(value = {AuthenticationCredentialsNotFoundException.class})
    protected ResponseEntity<ResponseError> authenticationCredentialsNotFoundExceptionHandler(
            AuthenticationCredentialsNotFoundException e, WebRequest request) {
        logPrinter.printLogException(request, HttpStatus.BAD_REQUEST, Error.MISSING_REQUEST_HEADER);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ResponseError(HttpStatus.BAD_REQUEST, Error.MISSING_REQUEST_HEADER));
    }

    @ExceptionHandler(value = {AccessDeniedException.class})
    protected ResponseEntity<ResponseError> accessDeniedExceptionHandler(AccessDeniedException e,
                                                                         WebRequest request) {
        logPrinter.printLogException(request, HttpStatus.BAD_REQUEST, Error.ACCESS_DENIED_ROLE);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ResponseError(HttpStatus.BAD_REQUEST, Error.ACCESS_DENIED_ROLE));
    }

    @ExceptionHandler(value = {NoValidDirectionException.class})
    protected ResponseEntity<ResponseError> noValidDirectionExceptionHandler(NoValidDirectionException e,
                                                                             WebRequest request) {
        logPrinter.printLogException(request, HttpStatus.BAD_REQUEST, Error.NO_VALID_DIRECTION);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ResponseError(HttpStatus.BAD_REQUEST, Error.NO_VALID_DIRECTION));
    }

    @ExceptionHandler(value = {NoValidSortMovieFieldException.class})
    protected ResponseEntity<ResponseError> noValidSortMovieFieldExceptionHandler(NoValidSortMovieFieldException e,
                                                                             WebRequest request) {
        logPrinter.printLogException(request, HttpStatus.BAD_REQUEST, Error.NO_VALID_MOVIE_FIELD);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ResponseError(HttpStatus.BAD_REQUEST, Error.NO_VALID_MOVIE_FIELD));
    }
}
