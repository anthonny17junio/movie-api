package com.api.configuration.exception;

import org.springframework.stereotype.Component;

@Component
public class NoValidDirectionException extends Exception {
}
