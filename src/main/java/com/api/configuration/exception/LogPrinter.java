package com.api.configuration.exception;

import com.api.model.Error;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.WebRequest;


/**
 * Singleton que se encarga de imprimir logs
 */
@Component
@Scope("singleton")
public class LogPrinter {

    private static final Logger LOGGER = LogManager.getLogger(LogPrinter.class);

    /**
     * Imprime una excepción en el log
     *
     * @param request    Request que tiene como header el common name necesario para imprimir en el log
     * @param httpStatus Estado http a imprimir en el log, si es de tipo BAD_REQUEST se imprimirá como warning, caso
     *                   contrario como error
     * @param error      Error a imprimir en el log
     */
    public void printLogException(WebRequest request, HttpStatus httpStatus, Error error) {
        printLogException(httpStatus, error);
    }

    /**
     * Imprime una excepción en el log
     *
     * @param httpStatus Estado http a imprimir en el log, si es de tipo BAD_REQUEST se imprimirá como warning, caso
     *                   contrario como error
     * @param error      Error a imprimir en el log
     */
    public void printLogException(HttpStatus httpStatus, Error error) {
        String message = error.getMessage()
            + " | Http Status: " + httpStatus;
        if (HttpStatus.BAD_REQUEST.equals(httpStatus)) {
            LOGGER.warn(message);
        } else {
            LOGGER.error(message);
        }
    }
}
