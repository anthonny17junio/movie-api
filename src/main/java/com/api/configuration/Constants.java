package com.api.configuration;

public class Constants {

    // Package names
    public static final String PACKAGE_MODEL = "com.api.model";
    public static final String PACKAGE_CONTROLLER = "com.api.controller";

    // Object names
    public static final String MOVIE = "Movies";
    public static final String GENRE = "Genre";
    public static final String USER = "User";
    public static final String RESPONSE_ERROR = "Response Error";
    public static final String ERROR_ELEMENT = "Error Element";
    public static final String TOKEN = "Token";

    // Table names
    public static final String TABLE_USERS = "general_users";
    public static final String TABLE_ROLES = "general_roles";
    public static final String TABLE_USERS_ROLES = "general_users_roles";
    public static final String TABLE_MOVIES = "movies";
    public static final String TABLE_GENRES = "genres";

    // URL Endpoints
    public static final String URL_TOKEN = "/token";
    public static final String URL_GENRE = "/genre";
    public static final String URL_MOVIE = "/movie";

    //MOVIE SORT FIELDS
    public static final String MOVIE_ID = "id";
    public static final String MOVIE_YEAR = "year";
    public static final String MOVIE_TITLE = "title";
    public static final String MOVIE_RATING = "rating";
    public static final String MOVIE_GENRE = "genre";

    //SORT TYPE
    public static final String SORT_ASC = "asc";
    public static final String SORT_DESC = "desc";

    //Default values
    public static final String DEFAULT_PAGE = "0";
    public static final String DEFAULT_PAGE_SIZE = "10";
    public static final int MAX_PAGE_SIZE = 100;

}
