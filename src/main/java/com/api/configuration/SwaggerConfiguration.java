package com.api.configuration;

import com.api.properties.SecurityProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.RequestParameterBuilder;
import springfox.documentation.builders.ResponseBuilder;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Autowired
    private SecurityProperties securityProperties;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.basePackage(Constants.PACKAGE_CONTROLLER))
            .paths(PathSelectors.ant("/**"))
            .build()
            .apiInfo(getApiInformation())
            .useDefaultResponseMessages(false)
            .genericModelSubstitutes(ResponseEntity.class)
            .useDefaultResponseMessages(false)
            .securitySchemes(Arrays.asList(apiKey()))
            .ignoredParameterTypes(HttpSession.class, HttpServletRequest.class, HttpServletResponse.class)
            .globalResponses(
                HttpMethod.GET,
                getCustomizedGetResponseMessages()
            ).globalResponses(
                HttpMethod.POST,
                getCustomizedPostResponseMessages()
            ).globalRequestParameters(Collections.singletonList(new RequestParameterBuilder()
                .name("Authorization")
                .description("Valid token")
                .in(ParameterType.HEADER)
                .required(true)
                .build()));
    }

    private List<Response> getCustomizedGetResponseMessages() {
        List<Response> responseMessages = new ArrayList<>();
        responseMessages.add(new ResponseBuilder()
            .code(String.valueOf(HttpStatus.UNAUTHORIZED.value()))
            .description(HttpStatus.UNAUTHORIZED.getReasonPhrase())
            .build());
        responseMessages.add(new ResponseBuilder()
            .code(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()))
            .description(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())
            .build());
        return responseMessages;
    }

    private List<Response> getCustomizedPostResponseMessages() {
        List<Response> responseMessages = new ArrayList<>();
        responseMessages.add(new ResponseBuilder()
            .code(String.valueOf(HttpStatus.UNAUTHORIZED.value()))
            .description(HttpStatus.UNAUTHORIZED.getReasonPhrase())
            .build());
        responseMessages.add(new ResponseBuilder()
            .code(String.valueOf(HttpStatus.BAD_REQUEST.value()))
            .description(HttpStatus.BAD_REQUEST.getReasonPhrase())
            .build());
        responseMessages.add(new ResponseBuilder()
            .code(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()))
            .description(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())
            .build());
        return responseMessages;
    }

    private ApiInfo getApiInformation() {
        return new ApiInfo("API Movies",
            "API REST - Movies",
            "1.0",
            "https://www.api.movies.com",
            new Contact("Anthonny Pazmiño", "https://www.linkedin.com/in/anthonny-pazmino-haro/", "anthonny17junio@gmail.com"),
            "API License",
            "https://www.licence.api.movies.com",
            Collections.emptyList()
        );
    }

    private ApiKey apiKey() {
        return new ApiKey("JWT", "Authorization", "header");
    }

}
