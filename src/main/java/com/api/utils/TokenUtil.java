package com.api.utils;

import com.api.properties.SecurityProperties;
import com.api.model.Token;
import com.api.model.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class TokenUtil {

    @Autowired
    private SecurityProperties securityProperties;

    public String getCnFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    public String getUserIdFromToken(String token) {
        String userId =
            Jwts.parser().setSigningKey(securityProperties.getSecret().getBytes(StandardCharsets.UTF_8)).parseClaimsJws(token).getHeader().get("UserId").toString();
        return userId;
    }

    public Date getIssuedAtDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getIssuedAt);
    }

    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(securityProperties.getSecret().getBytes(StandardCharsets.UTF_8)).parseClaimsJws(token).getBody();
    }

    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    private Boolean ignoreTokenExpiration(String token) {
        // here you specify tokens, for that the expiration is ignored
        return false;
    }

    public Token doGenerateToken(User user) {
        ZonedDateTime issuedAt = Instant.now().atZone(ZoneOffset.UTC);
        ZonedDateTime expiration = Instant.now().plus(Integer.parseInt(securityProperties.getTokenExpiryTime()), ChronoUnit.MILLIS).atZone(ZoneOffset.UTC);

        String generated = Jwts
            .builder()
            .setId(securityProperties.getJwtId())
            .setSubject(user.getCn())
            .setHeaderParam("UserId", user.getId())
            .claim("authorities",
                user.getRoles().stream().map(role -> (GrantedAuthority) role::getName).collect(Collectors.toList()))
            .setIssuedAt(Date.from(issuedAt.toInstant()))
            .setExpiration(Date.from(expiration.toInstant()))
            .signWith(SignatureAlgorithm.HS512,
                securityProperties.getSecret().getBytes(StandardCharsets.UTF_8)).compact();

        Token token = Token.builder()
            .issuedAt(issuedAt)
            .expiration(expiration)
            .token("Bearer " + generated).build();

        return token;
    }

    public Boolean canTokenBeRefreshed(String token) {
        return (!isTokenExpired(token) || ignoreTokenExpiration(token));
    }

    public Boolean validateToken(String token, String cn, User user) {
        return (cn.equals(user.getUsername()) && !isTokenExpired(token));
    }

}
