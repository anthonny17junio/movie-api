package com.api.repository;

import com.api.model.Genre;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repositorio de géneros
 */
public interface GenreRepository extends JpaRepository<Genre, Long> {
}
