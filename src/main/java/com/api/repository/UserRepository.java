package com.api.repository;

import com.api.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repositorio de Token
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * Obtiene un usuario dado el nombre de usuario
     *
     * @param userName nombre de usuario
     * @return Usuario encontrado o null
     */
    User findUserByCnEqualsAndActiveIsTrue(String userName);
}
