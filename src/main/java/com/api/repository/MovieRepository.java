package com.api.repository;

import com.api.model.Movie;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Repositorio de películas
 */
public interface MovieRepository extends JpaRepository<Movie, Long> {
    /**
     * Obtiene un listado de películas filtrando por un listado de géneros
     *
     * @param genreIdList Listado de géneros a filtrar
     * @param pageable    Página a obtener
     * @return Listado de películas
     */
    List<Movie> findByGenreIdIn(Long[] genreIdList, Pageable pageable);
}
