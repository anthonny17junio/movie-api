package com.api.service;

import com.api.configuration.exception.GenreNotFoundException;
import com.api.configuration.exception.NoValidDirectionException;
import com.api.configuration.exception.NoValidSortMovieFieldException;
import com.api.configuration.exception.UnprocessableEntityException;
import com.api.dto.InsertMovieDto;
import com.api.dto.MovieDto;
import com.api.model.Genre;
import com.api.model.Movie;
import com.api.repository.GenreRepository;
import com.api.repository.MovieRepository;
import lombok.Getter;
import lombok.Setter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.api.configuration.Constants.*;

/**
 * Servicio de películas
 */
@Service
@Transactional
@Getter
@Setter
public class MovieService {
    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private ModelMapper modelMapperModelToDto;

    @Autowired
    private ModelMapper modelMapperDtoToModel;

    private List<String> validMovieFieldList = Arrays.asList(MOVIE_ID, MOVIE_YEAR, MOVIE_TITLE, MOVIE_RATING,
            MOVIE_GENRE);


    /**
     * Obtiene una película dado un id
     *
     * @param id Id del película a buscar
     * @return Respuesta HTTP con el película, devolverá un status NOT_FOUND si no existe una película con dicho id
     */
    public ResponseEntity<MovieDto> getById(long id) {
        Optional<Movie> movie = movieRepository.findById(id);
        if (movie.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(modelMapperModelToDto.map(movie.get(), MovieDto.class));
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    /**
     * Inserta una película en BBDD
     *
     * @param movieDto Película a insertar
     * @return Respuesta HTTP, devolverá un HTTP OK si se insertó correctamente
     * @throws UnprocessableEntityException Si existe algún error al procesar la entidad
     * @throws GenreNotFoundException       Si el género indicado no existe
     */
    public ResponseEntity<Object> createMovie(InsertMovieDto movieDto) throws UnprocessableEntityException, GenreNotFoundException {
        Movie movieToInsert = modelMapperDtoToModel.map(movieDto, Movie.class);
        Optional<Genre> genre = genreRepository.findById(movieDto.getGenreId());
        if (!genre.isPresent()) {
            throw new GenreNotFoundException();
        }
        movieToInsert.setId(null);
        Movie insertedMovie = movieRepository.save(movieToInsert);
        if (insertedMovie != null) {
            return ResponseEntity.ok().body(insertedMovie.getId());
        }
        throw new UnprocessableEntityException();
    }

    /**
     * Elimina una película por id
     *
     * @param id Id del película a eliminar
     * @return Respuesta HTTP, devolverá un HTTP OK si se eliminó, y un HTTP NOT FOUND si no existe dicho id
     */
    public ResponseEntity<Void> deleteById(long id) {
        Optional<Movie> movie = movieRepository.findById(id);
        if (movie.isPresent()) {
            movieRepository.deleteById(id);
            return ResponseEntity.status(HttpStatus.OK).build();
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    /**
     * Obtiene un listado de películas de modo paginado
     *
     * @param sortList         Lista de campos y tipo (asc,desc) de orden a aplicar en la paginación.
     *                         Sigue el siguiente formato: ["campo1,tipo1","campo2,tipo2"]
     * @param genresFilterList Lista de id de géneros a filtrar
     * @param pageNumber       Número de página (0 por defecto)
     * @param size             Número de elementos por página (10 por defecto, 100 máximo)
     * @return
     * @throws NoValidDirectionException      Si el tipo de orden (asc,desc) no es correcto
     * @throws NoValidSortMovieFieldException Si el campo al que aplicar el orden no existe
     */
    public ResponseEntity<List<MovieDto>> getMovies(
            String[] sortList, Optional<Long[]> genresFilterList, int pageNumber, int size)
            throws NoValidDirectionException, NoValidSortMovieFieldException {
        List<Movie> moviesToReturnList;
        Pageable page = createPage(sortList, pageNumber, size);

        if (genresFilterList.isPresent()) {
            moviesToReturnList = movieRepository.findByGenreIdIn(genresFilterList.get(), page);
        } else {
            moviesToReturnList = movieRepository.findAll(page).toList();
        }

        if (moviesToReturnList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return ResponseEntity.status(HttpStatus.OK)
                .body(moviesToReturnList.stream().map(
                        movie -> modelMapperModelToDto.map(movie, MovieDto.class)
                ).collect(Collectors.toList()));
    }

    /**
     * Crea una página dado un listado de campos de orden, el número de página y el tamaño de página
     *
     * @param sortList   Listado de campos de orden
     * @param pageNumber Número de página
     * @param size       Tamapo de página
     * @return Página
     * @throws NoValidDirectionException      Si el tipo de orden (asc,desc) no es correcto
     * @throws NoValidSortMovieFieldException Si el campo al que aplicar el orden no existe
     */
    private Pageable createPage(String[] sortList, int pageNumber, int size)
            throws NoValidDirectionException, NoValidSortMovieFieldException {
        List<Sort.Order> castedSortList = new ArrayList<>();
        if (sortList[0].contains(",")) {
            for (String sortToCast : sortList) {
                String[] castedSort = sortToCast.split(",");
                validateSortMovieField(castedSort[0]);
                castedSortList.add(new Sort.Order(getSortDirection(castedSort[1]), castedSort[0]));
            }
        } else {
            validateSortMovieField(sortList[0]);
            castedSortList.add(new Sort.Order(getSortDirection(sortList[1]), sortList[0]));
        }

        Pageable pageable = PageRequest.of(pageNumber, size, Sort.by(castedSortList));
        return pageable;
    }

    /**
     * Valida que un campo de orden sea válido (que exista en la clase Movie)
     *
     * @param movieField campo de orden
     * @throws NoValidSortMovieFieldException si no es válido
     */
    private void validateSortMovieField(String movieField) throws NoValidSortMovieFieldException {
        if (!validMovieFieldList.contains(movieField)) {
            throw new NoValidSortMovieFieldException();
        }
    }


    /**
     * Obtiene la dirección de un orden (asc, desc)
     *
     * @param direction String de dirección a obtener
     * @return Tipo de dirección de orden
     * @throws NoValidDirectionException si no es válido
     */
    private Sort.Direction getSortDirection(String direction) throws NoValidDirectionException {
        if ((SORT_ASC).equalsIgnoreCase(direction)) {
            return Sort.Direction.ASC;
        } else if ((SORT_DESC).equalsIgnoreCase(direction)) {
            return Sort.Direction.DESC;
        } else {
            throw new NoValidDirectionException();
        }
    }
}
