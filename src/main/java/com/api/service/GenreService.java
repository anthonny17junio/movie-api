package com.api.service;

import com.api.configuration.exception.UnprocessableEntityException;
import com.api.dto.GenreDto;
import com.api.dto.InsertGenreDto;
import com.api.model.Genre;
import com.api.repository.GenreRepository;
import lombok.Getter;
import lombok.Setter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Servicio de género
 */
@Service
@Transactional
@Getter
@Setter
public class GenreService {
    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private ModelMapper modelMapperModelToDto;

    @Autowired
    private ModelMapper modelMapperDtoToModel;

    /**
     * Obtiene todos los géneros
     *
     * @return Lista de géneros
     */
    public ResponseEntity<List<GenreDto>> getAll() {
        List<Genre> genreList = genreRepository.findAll();
        return ResponseEntity.status(HttpStatus.OK)
                .body(genreList.stream().map(
                        genre -> modelMapperModelToDto.map(genre, GenreDto.class)
                ).collect(Collectors.toList()));
    }

    /**
     * Obtiene un género dado un id
     *
     * @param id Id del género a buscar
     * @return Respuesta HTTP con el género, devolverá un status NOT_FOUND si no existe un género con dicho id
     */
    public ResponseEntity<GenreDto> getById(long id) {
        Optional<Genre> genre = genreRepository.findById(id);
        if (genre.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(modelMapperModelToDto.map(genre.get(), GenreDto.class));
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    /**
     * Inserta un género en BBDD
     *
     * @param genreDto Género a insertar
     * @return Respuesta HTTP, devolverá un HTTP OK si se insertó correctamente
     * @throws UnprocessableEntityException
     */
    public ResponseEntity<Object> createGenre(InsertGenreDto genreDto) throws UnprocessableEntityException {
        Genre genreToInsert = modelMapperDtoToModel.map(genreDto, Genre.class);
        genreToInsert.setId(null);
        Genre insertedGenre = genreRepository.save(genreToInsert);
        if (insertedGenre != null) {
            return ResponseEntity.ok().body(insertedGenre.getId());
        }
        throw new UnprocessableEntityException();
    }

    /**
     * Elimina un género por id
     *
     * @param id Id del género a eliminar
     * @return Respuesta HTTP, devolverá un HTTP OK si se eliminó, y un HTTP NOT FOUND si no existe dicho id
     */
    public ResponseEntity<Void> deleteById(long id) {
        Optional<Genre> genre = genreRepository.findById(id);
        if (genre.isPresent()) {
            genreRepository.deleteById(id);
            return ResponseEntity.status(HttpStatus.OK).build();
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}
