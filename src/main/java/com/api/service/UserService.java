package com.api.service;

import com.api.properties.SecurityProperties;
import com.api.utils.TokenUtil;
import com.api.model.Token;
import com.api.model.User;
import com.api.repository.UserRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@Getter
@Setter
public class UserService implements UserDetailsService {

    @Autowired
    private SecurityProperties securityProperties;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TokenUtil tokenUtil;

    @Override
    public User loadUserByUsername(String cn) throws UsernameNotFoundException {
        User user = findUserByCnAndIsActive(cn);
        if (user == null) {
            throw new UsernameNotFoundException("");
        }
        return user;
    }

    public User findUserByCnAndIsActive(String cn) {
        return userRepository.findUserByCnEqualsAndActiveIsTrue(cn);
    }

    public Token getJWTToken(User user) {
        return tokenUtil.doGenerateToken(user);
    }

}
