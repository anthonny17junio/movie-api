package com.api.service;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface IService<T> {
    List<T> getAll();

    Optional<T> getById(long id);

    long count();
}
