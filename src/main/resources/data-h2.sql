INSERT INTO general_users (id, cn, name, active)
VALUES (1, 'admin.movies.com', 'Anthonny', true);
INSERT INTO general_users (id, cn, name, active)
VALUES (2, 'user_pepe.movies.com', 'Pepe', true);

INSERT INTO general_roles (id, name)
VALUES (1, 'ROLE_USER');
INSERT INTO general_roles (id, name)
VALUES (2, 'ROLE_ADMIN');

INSERT INTO general_users_roles (user_id, role_id)
VALUES (1, 1);
INSERT INTO general_users_roles (user_id, role_id)
VALUES (1, 2);
INSERT INTO general_users_roles (user_id, role_id)
VALUES (2, 1);

INSERT INTO genres (id, name)
VALUES (1, 'Action');
INSERT INTO genres (id, name)
VALUES (2, 'Terror');
INSERT INTO genres (id, name)
VALUES (3, 'Comedy');
INSERT INTO genres (id, name)
VALUES (4, 'Other');

INSERT INTO movies(id, title, rating, year,genre_id)
VALUES (1,'Fantasy Island',4.3,2010,2);
INSERT INTO movies(id, title, rating, year,genre_id)
VALUES (2,'Escape room',2.1,2010,2);
INSERT INTO movies(id, title, rating, year,genre_id)
VALUES (3,'The deep house',3.6,2010,2);
INSERT INTO movies(id, title, rating, year,genre_id)
VALUES (4,'Wrong turn',5.0,2010,2);
INSERT INTO movies(id, title, rating, year,genre_id)
VALUES (5,'Hereditary',2.5,2010,2);
INSERT INTO movies(id, title, rating, year,genre_id)
VALUES (6,'A quiet place',4.5,2011,2);
INSERT INTO movies(id, title, rating, year,genre_id)
VALUES (7,'Split',3.7,2011,2);
INSERT INTO movies(id, title, rating, year,genre_id)
VALUES (8,'Mother',2.4,2011,2);
INSERT INTO movies(id, title, rating, year,genre_id)
VALUES (9,'It',4.9,2011,2);
INSERT INTO movies(id, title, rating, year,genre_id)
VALUES (10,'Child play',2.3,2009,2);
INSERT INTO movies(id, title, rating, year,genre_id)
VALUES (11,'The visit',4.9,2008,2);
INSERT INTO movies(id, title, rating, year,genre_id)
VALUES (12,'Overlord',3.4,2007,2);
INSERT INTO movies(id, title, rating, year,genre_id)
VALUES (13,'Get out',1.7,2007,2);
INSERT INTO movies(id, title, rating, year,genre_id)
VALUES (14,'Free guy',1.7,2006,3);
INSERT INTO movies(id, title, rating, year,genre_id)
VALUES (15,'Bad boy',3.7,2020,3);
INSERT INTO movies(id, title, rating, year,genre_id)
VALUES (16,'The nice guys',4.7,2012,3);
INSERT INTO movies(id, title, rating, year,genre_id)
VALUES (17,'American assassin',2.7,2001,1);
INSERT INTO movies(id, title, rating, year,genre_id)
VALUES (18,'Call me by your name',3.7,2010,4);

