package com.api.repository;

import com.api.model.Genre;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;

@DataJpaTest
@ActiveProfiles("develop")
class GenreRepositoryTest {
    @Autowired
    GenreRepository genreRepository;

    @Test
    void findAll() {
        List<Genre> genreList = genreRepository.findAll();
        Assert.assertTrue(genreList != null && genreList.size() > 0);
    }

    @Test
    void findByIdNotNull() {
        Optional<Genre> genre = genreRepository.findById(1L);
        Assert.assertNotNull(genre.get());
    }

    @Test
    void findByIdNull() {
        Optional<Genre> genre = genreRepository.findById(-1L);
        Assert.assertFalse(genre.isPresent());
    }

    @Test
    void insert() {
        Genre genre = new Genre();
        genre.setName("NAME 1");
        Genre insertedGenre = genreRepository.save(genre);
        Assert.assertNotNull(insertedGenre);
    }

    @Test
    void delete() {
        Genre genre = new Genre();
        genre.setName("NAME 2");
        Genre insertedGenre = genreRepository.save(genre);
        genreRepository.deleteById(insertedGenre.getId());
        Optional<Genre> deletedGenre = genreRepository.findById(insertedGenre.getId());
        Assert.assertFalse(deletedGenre.isPresent());
    }
}