package com.api.repository;

import com.api.model.Genre;
import com.api.model.Movie;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;

@DataJpaTest
@ActiveProfiles("develop")
class MovieRepositoryTest {
    @Autowired
    MovieRepository movieRepository;

    @Autowired
    GenreRepository genreRepository;

    @Test
    void findByIdNotNull() {
        Optional<Movie> movie = movieRepository.findById(1L);
        Assert.assertNotNull(movie.get());
    }

    @Test
    void findByIdNull() {
        Optional<Movie> movie = movieRepository.findById(-1L);
        Assert.assertFalse(movie.isPresent());
    }

    @Test
    void insert() {
        Movie movie = new Movie();
        Optional<Genre> genre = genreRepository.findById(1L);
        movie.setGenre(genre.get());
        movie.setRating(5.0F);
        movie.setTitle("Movie title");
        movie.setYear(2000);
        Movie insertedMovie = movieRepository.save(movie);
        Assert.assertNotNull(insertedMovie);
    }

    @Test
    void delete() {
        Movie movie = new Movie();
        Optional<Genre> genre = genreRepository.findById(1L);
        movie.setGenre(genre.get());
        movie.setRating(5.0F);
        movie.setTitle("Movie title");
        movie.setYear(2000);
        Movie insertedMovie = movieRepository.save(movie);
        movieRepository.deleteById(insertedMovie.getId());
        Optional<Movie> deletedMovie = movieRepository.findById(insertedMovie.getId());
        Assert.assertFalse(deletedMovie.isPresent());
    }

    @Test
    void findByGenreIdIn() {
        Long[] genreIdList = new Long[]{1L, 2L, 3L, 4L};
        Pageable pageable = PageRequest.of(0, 5);
        List<Movie> insertedMovie = movieRepository.findByGenreIdIn(genreIdList, pageable);
        Assert.assertTrue(insertedMovie.size() == 5);
    }

}