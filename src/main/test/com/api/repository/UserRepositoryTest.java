package com.api.repository;

import com.api.model.User;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

@DataJpaTest
@ActiveProfiles("develop")
class UserRepositoryTest {

    @Autowired
    UserRepository userRepository;

    @Test
    void findUserByCnEqualsAndActiveIsTrueNullTest() {
        User user = userRepository.findUserByCnEqualsAndActiveIsTrue("abc");
        Assert.assertNull(user);
    }

    @Test
    void findUserByCnEqualsAndActiveIsTrueNotNullTest() {
        User user = userRepository.findUserByCnEqualsAndActiveIsTrue("admin.movies.com");
        Assert.assertNotNull(user);
    }
}